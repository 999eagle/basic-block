mod graph;

pub fn add_subcmds<'a, 'b>(app: clap::App<'a, 'b>) -> clap::App<'a, 'b> {
    app.subcommand(graph::Graph::subcommand())
}

pub fn match_subcmd(matches: &clap::ArgMatches) -> anyhow::Result<()> {
    match matches.subcommand() {
        (name, Some(submatches)) if name == graph::Graph::name() => {
            graph::Graph::parse_config(submatches).and_then(|c| graph::Graph::run(&c))
        }
        _ => {
            println!("{}", matches.usage());
            Ok(())
        }
    }
}

pub trait Command {
    type Config;

    fn name() -> &'static str;
    fn subcommand<'a, 'b>() -> clap::App<'a, 'b>;

    fn parse_config(matches: &clap::ArgMatches) -> anyhow::Result<Self::Config>;
    fn run(config: &Self::Config) -> anyhow::Result<()>;
}
