mod command;
mod config;
mod io;
mod model;
mod util;

fn main() {
    let app = config::build_clap_app();
    let app = command::add_subcmds(app);
    let matches = app.get_matches();
    let verbosity = config::get_verbosity(&matches) + 3; // default to level 3 (Info)
    util::setup_log(verbosity);

    let result = command::match_subcmd(&matches);

    if let Err(e) = result {
        log::error!("{:#}", e);
    }
}
