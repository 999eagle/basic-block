use clap::{App, Arg, ArgMatches};

pub fn get_verbosity(matches: &ArgMatches) -> i8 {
    matches.occurrences_of("verbose") as i8 - matches.occurrences_of("quiet") as i8
}

pub fn build_clap_app<'a, 'b>() -> App<'a, 'b> {
    App::new("basic-block")
        .version(version::version!())
        .author("Sophie Tauchert")
        .arg(
            Arg::with_name("verbose")
                .help("Increase verbosity. May be specified multiple times.")
                .short("v")
                .long("verbose")
                .multiple(true)
                .conflicts_with("quiet")
                .global(true),
        )
        .arg(
            Arg::with_name("quiet")
                .help("Decrease verbosity. May be specified multiple times.")
                .short("q")
                .long("quiet")
                .multiple(true)
                .global(true),
        )
}
