use anyhow::Context;
use simple_logger::SimpleLogger;
use std::ffi::OsStr;
use std::path::PathBuf;

pub fn setup_log(verbosity: i8) {
    #[allow(clippy::wildcard_in_or_patterns)]
    let level = match verbosity {
        0 => log::LevelFilter::Off,
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 | _ => log::LevelFilter::Trace,
    };
    if SimpleLogger::new()
        .with_level(level)
        .with_timestamps(true)
        .with_colors(true)
        .init()
        .is_err()
    {
        log::warn!("Failed to set logger, a logger has already been set!")
    }
}

pub fn map_input(val: &OsStr) -> crate::io::Input {
    if val == "-" {
        crate::io::Input::Stdin
    } else {
        crate::io::Input::File(PathBuf::from(val))
    }
}

pub fn open_input(input: &crate::io::Input) -> anyhow::Result<Box<dyn std::io::Read>> {
    Ok(match &input {
        crate::io::Input::File(path) => Box::new(
            std::fs::OpenOptions::new()
                .read(true)
                .open(path)
                .context("Failed to open input file")?,
        ),
        crate::io::Input::Stdin => Box::new(std::io::stdin()),
    })
}
