use std::path::PathBuf;

pub enum Input {
    File(PathBuf),
    Stdin,
}
