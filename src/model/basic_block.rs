use anyhow::Context;
use regex::{Match, Regex};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::iter::Peekable;
use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct BasicBlock {
    label: LabelNumber,
    tuples: Vec<Tuple>,
}

#[derive(Debug, Clone)]
pub struct Tuple {
    tuple_number: Option<TupleNumber>,
    instruction: Instruction,
}

#[derive(Debug, Clone)]
pub enum Instruction {
    Jump(LabelNumber),
    Branch(BranchType, TupleNumber, LabelNumber, LabelNumber),
    Other(String),
}

#[derive(Debug, Copy, Clone)]
pub enum BranchType {
    GreaterThan,
    LessThan,
    GreaterThanEqual,
    LessThanEqual,
    Equal,
    NotEqual,
}

#[derive(Debug, Copy, Clone)]
pub struct TupleNumber(usize);

#[derive(Debug, Copy, Clone)]
pub struct LabelNumber(usize);

impl BasicBlock {
    pub fn label(&self) -> &LabelNumber {
        &self.label
    }

    pub fn tuples(&self) -> &Vec<Tuple> {
        &self.tuples
    }

    pub fn jump_targets(&self) -> Vec<LabelNumber> {
        let mut jumps = Vec::new();
        for tuple in &self.tuples {
            match tuple.instruction() {
                Instruction::Jump(l) => jumps.push(*l),
                Instruction::Branch(_, _, l0, l1) => {
                    jumps.push(*l0);
                    jumps.push(*l1);
                }
                _ => {}
            }
        }
        jumps
    }

    pub fn read<I: Iterator<Item = Result<String, E>>, E: 'static + Error + Send + Sync>(
        read: &mut Peekable<I>,
    ) -> anyhow::Result<Self> {
        lazy_static::lazy_static! {
            static ref LABEL_REGEX: Regex = Regex::new(r"^L([0-9]+)\s*:\s*$").unwrap();
            static ref INSTR_REGEX: Regex = Regex::new(r"^(t(?P<tuple>[0-9]+)\s*:\s*(?P<instr>.*)|(?P<static_instr>RET))\s*\n?$").unwrap();
        };

        let label = if let Some(label) = read.next() {
            label.context("Failed to read label")?
        } else {
            // EOF
            return Err(anyhow::anyhow!("EOF"));
        };
        let label = if let Some(label) = LABEL_REGEX
            .captures(&label)
            .and_then(|caps| caps.get(1).map(|m| m.as_str()))
        {
            LabelNumber(usize::from_str(label)?)
        } else {
            return Err(anyhow::anyhow!("Invalid label specification: {:?}", label));
        };

        let mut instructions = Vec::new();
        loop {
            let instr = match read.peek() {
                Some(Ok(instr)) => instr,
                None => break,
                Some(Err(_)) => {
                    return Err(read
                        .next()
                        .unwrap()
                        .context("Failed to read instruction")
                        .unwrap_err());
                }
            };
            let instr = if let Some(caps) = INSTR_REGEX.captures(instr) {
                match (
                    caps.name("tuple"),
                    caps.name("instr"),
                    caps.name("static_instr"),
                ) {
                    (Some(tuple), Some(instr), None) => Tuple {
                        tuple_number: Some(TupleNumber(
                            usize::from_str(tuple.as_str())
                                .context("Failed to parse tuple number")?,
                        )),
                        instruction: Instruction::from_str(instr.as_str())?,
                    },
                    (None, None, Some(instr)) => Tuple {
                        tuple_number: None,
                        instruction: Instruction::from_str(instr.as_str())?,
                    },
                    _ => {
                        return Err(anyhow::anyhow!("Failed to parse instruction {:?}", instr));
                    }
                }
            } else {
                // didn't match, so stop reading
                break;
            };
            // valid instruction -> advance the iterator
            read.next();
            instructions.push(instr);
        }

        Ok(Self {
            label,
            tuples: instructions,
        })
    }
}

impl Tuple {
    pub fn tuple_number(&self) -> Option<&TupleNumber> {
        self.tuple_number.as_ref()
    }

    pub fn instruction(&self) -> &Instruction {
        &self.instruction
    }
}

impl TupleNumber {
    pub fn get(&self) -> usize {
        self.0
    }
}

impl LabelNumber {
    pub fn get(&self) -> usize {
        self.0
    }
}

impl FromStr for Instruction {
    type Err = anyhow::Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        lazy_static::lazy_static! {
            static ref JMP_REGEX: Regex = Regex::new(r"^JMP\s+L([0-9]+)$").unwrap();
            static ref BRANCH_REGEX: Regex = Regex::new(
                r"^(BGT|BLT|BGE|BLE|BEQ|BNE)\s+t([0-9]+)\s+L([0-9]+)\s+L([0-9]+)$"
            )
            .unwrap();
        };
        fn unwrap_match(m: Option<Match>) -> anyhow::Result<&str> {
            Ok(m.ok_or_else(|| anyhow::anyhow!("Match failed"))?.as_str())
        }
        fn parse_tuple(m: Option<Match>) -> anyhow::Result<TupleNumber> {
            Ok(TupleNumber(
                usize::from_str(unwrap_match(m)?)
                    .with_context(|| format!("Failed to parse tuple number from {:?}", m))?,
            ))
        }
        fn parse_label(m: Option<Match>) -> anyhow::Result<LabelNumber> {
            Ok(LabelNumber(
                usize::from_str(unwrap_match(m)?)
                    .with_context(|| format!("Failed to parse label number from {:?}", m))?,
            ))
        }

        if let Some(jmp) = JMP_REGEX.captures(str) {
            return Ok(Self::Jump(parse_label(jmp.get(1))?));
        }
        if let Some(branch) = BRANCH_REGEX.captures(str) {
            return Ok(Self::Branch(
                BranchType::from_str(unwrap_match(branch.get(1))?)?,
                parse_tuple(branch.get(2))?,
                parse_label(branch.get(3))?,
                parse_label(branch.get(4))?,
            ));
        }

        Ok(Self::Other(str.to_owned()))
    }
}

impl Display for Tuple {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(t) = &self.tuple_number {
            f.write_fmt(format_args!("t{}: {}", t.0, self.instruction))
        } else {
            f.write_fmt(format_args!("{}", self.instruction))
        }
    }
}

impl Display for Instruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Instruction::Jump(l) => f.write_fmt(format_args!("JMP L{}", l.0)),
            Instruction::Branch(bt, t, l0, l1) => {
                f.write_fmt(format_args!("{} t{} L{} L{}", bt, t.0, l0.0, l1.0))
            }
            Instruction::Other(s) => f.write_str(s),
        }
    }
}

impl FromStr for BranchType {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "BGT" => Ok(Self::GreaterThan),
            "BLT" => Ok(Self::LessThan),
            "BGE" => Ok(Self::GreaterThanEqual),
            "BLE" => Ok(Self::LessThanEqual),
            "BEQ" => Ok(Self::Equal),
            "BNE" => Ok(Self::NotEqual),
            _ => Err(anyhow::anyhow!("Invalid branch type: {:?}", s)),
        }
    }
}

impl Display for BranchType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            BranchType::GreaterThan => f.write_str("BGT"),
            BranchType::LessThan => f.write_str("BLT"),
            BranchType::GreaterThanEqual => f.write_str("BGE"),
            BranchType::LessThanEqual => f.write_str("BLE"),
            BranchType::Equal => f.write_str("BEQ"),
            BranchType::NotEqual => f.write_str("BNE"),
        }
    }
}
