use crate::model::BasicBlock;
use anyhow::Context;
use clap::{App, Arg, ArgMatches, SubCommand};
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};

pub struct Graph {}

pub struct GraphConfig {
    pub input: crate::io::Input,
    pub output: PathBuf,
}

impl super::Command for Graph {
    type Config = GraphConfig;

    fn name() -> &'static str {
        "graph"
    }

    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("graph")
            .about("Generate a basic block graph from a list of tuples.")
            .arg(
                Arg::with_name("OUTPUT_PATH")
                    .help("The output file path.")
                    .short("o")
                    .long("output")
                    .takes_value(true)
                    .required(true),
            )
            .arg(
                Arg::with_name("INPUT")
                    .help("The input file to use. Use - for stdin.")
                    .required(true)
                    .index(1)
                    .takes_value(true)
                    .validator_os(|val| {
                        if val == "-" {
                            return Ok(());
                        }
                        let path = Path::new(val);
                        if !path.exists() || !path.is_file() {
                            return Err(format!("File {:?} doesn't exist!", path).into());
                        }
                        Ok(())
                    }),
            )
    }

    fn parse_config(matches: &ArgMatches) -> anyhow::Result<Self::Config> {
        let output = matches
            .value_of_os("OUTPUT_PATH")
            .map(PathBuf::from)
            .ok_or_else(|| anyhow::anyhow!("Invalid output."))?;
        let input = matches
            .value_of_os("INPUT")
            .map(crate::util::map_input)
            .ok_or_else(|| anyhow::anyhow!("Invalid input."))?;
        Ok(Self::Config { output, input })
    }

    fn run(config: &Self::Config) -> anyhow::Result<()> {
        let input = crate::util::open_input(&config.input)?;
        let mut read = BufReader::new(input).lines().peekable();

        let mut blocks = Vec::new();
        while read.peek().is_some() {
            let basic_block = BasicBlock::read(&mut read)?;
            blocks.push(basic_block);
        }
        log::debug!("Read basic blocks: {:#?}", blocks);

        let file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&config.output)
            .context("Failed to open output")?;
        let mut writer = BufWriter::new(file);
        writer.write_all(
            r##"digraph{node[fontname="Fira Code",fillcolor="#FFFF00",style="filled"];"##
                .as_bytes(),
        )?;
        for block in &blocks {
            writer.write_fmt(format_args!(
                r#"L{}[shape=box,label="L{}:{}"];"#,
                block.label().get(),
                block.label().get(),
                block
                    .tuples()
                    .iter()
                    .fold(String::from("\\l"), |a, t| format!("{}{}\\l", a, t))
            ))?;
        }
        for block in &blocks {
            for target in block.jump_targets() {
                writer.write_fmt(format_args!(
                    "L{} -> L{};",
                    block.label().get(),
                    target.get()
                ))?;
            }
        }
        writer.write_all("}".as_bytes())?;
        writer.flush()?;
        Ok(())
    }
}
